//
//  Snow_WeatherApp.swift
//  Snow Weather
//
//  Created by PhD Hossein Payami on 2/12/23.
//

import SwiftUI

@main
struct Snow_WeatherApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
